class BankSystem {
  constructor() {
    this.queueList = [];
    this.customerList = [];
  }

  isCustomer(name) {
    let isCustomer = false;
    for (let i = 0; i < this.customerList.length; i++) {
      if (this.customerList[i][0] === name) {
        return isCustomer = true;
      }
    } return isCustomer;
  }

  setBalance(name, money) {
    for (let i = 0; i <= this.customerList.length; i++) {
      if (this.customerList[i][0] === name) {
        return this.customerList[i][1] += money;
      }
    }
  }

  getBalance(name) {
    for (let i = 0; i <= this.customerList.length; i++) {
      if (this.customerList[i][0] === name) {
        return this.customerList[i][1];
      }
    }
  }


  addQueueList(customer) {
    if (!this.isCustomer(customer.getName())) {
      this.queueList.push(customer);
      let temp = Object.values(customer);
      temp.splice(1,2);
      temp = temp.slice(0,2);
      this.customerList.push(temp);
    } else {
      this.queueList.push(customer);
    }
  }

  getQueueList() {
    return this.queueList;
  }

  removeQueueList() {
    return this.queueList.shift();
  }
}

class Person {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}. I'm from ${this.address}`);
  }
}

class Security extends Person {
  constructor(name, address) {
    super(name, address);
  }

  setQueueList(customer) {
    bankSystem.addQueueList(customer);
  }

  register(customer) {
    if (customer.getIsInAQueue()) {
      console.log(`Sorry, ${customer.getName()}, you're already in a queue list.`);
    } else if (customer.getIntent() == 'done') {
      console.log(`Your transaction is done`);
    } else {
      this.setQueueList(customer);
      customer.setIsInAQueue(1);
      console.log(`Congratulations, ${customer.getName()}. You're in queue number ${bankSystem.getQueueList().length}`);
    }
  }  
}

class Customer extends Person {
  constructor(name, address, intent, transactionValue, balance, transferDestination = 'No Transfer Destination') {
    super(name, address);
    this.intent = intent;
    this.balance = balance;
    this.transferDestination = transferDestination;
    this.transactionValue = transactionValue;
    this.isInAQueue = 0;
    this.isCustomer = 0;
  }

  getName() {
    return this.name;
  }
  
  setIntent() {
    this.intent = 'done';
  }

  getIntent() {
    return this.intent;
  }

  getTransactionValue() {
    return this.transactionValue;
  }

  setBalance(money) {
    this.balance += money;
  }

  getBalance() {
    return this.balance;
  }

  getTransferDestination() {
    return this.transferDestination;
  }

  setIsInAQueue(num) {
    this.isInAQueue += num;
  }

  getIsInAQueue() {
    return this.isInAQueue;
  }

  setIsCustomer() {
    this.isCustomer = 1;
  }

  getIsCustomer() {
    return this.isCustomer;
  }
}

class Teller extends Person {
  constructor(name, address, counter) {
    super(name, address);
    this.counter = counter;
  }
  
  serve() {
    if (bankSystem.getQueueList().length >= 1) {
      this.getNextOnQueueList().setIsInAQueue(-1);

      switch(this.getNextOnQueueList().getIntent()) {
        case 'withdraw':
          this.getNextOnQueueList().setIntent();
          if (bankSystem.getBalance(this.getNextOnQueueList().getName()) >= this.getNextOnQueueList().getTransactionValue()) {
            bankSystem.setBalance(this.getNextOnQueueList().getName(), -this.getNextOnQueueList().getTransactionValue());
            console.log(`You successfully withdraw ${this.getNextOnQueueList().getTransactionValue()}. You now have ${this.getNextOnQueueList().getBalance()} left in your account`);
          } else {
            console.log(`Sorry, you don't have sufficient funds in your account.`);
          }
          bankSystem.removeQueueList();
          break;
        
        case 'deposit':
          this.getNextOnQueueList().setIntent();
          bankSystem.setBalance(this.getNextOnQueueList().getName(), this.getNextOnQueueList().getTransactionValue());
          console.log(`You successfully deposit ${this.getNextOnQueueList().getTransactionValue()}. You now have ${bankSystem.getBalance(this.getNextOnQueueList().getName())} in your account.`);
          bankSystem.removeQueueList();
          break;
        
        case 'check':
          this.getNextOnQueueList().setIntent();
          console.log(`You have ${bankSystem.getBalance(this.getNextOnQueueList().getName())} in your account`);
          bankSystem.removeQueueList();
          break;

        case 'transfer':
          this.getNextOnQueueList().setIntent();
          if (bankSystem.isCustomer(this.getNextOnQueueList().getTransferDestination())) {
            if (bankSystem.getBalance(this.getNextOnQueueList().getName()) >= this.getNextOnQueueList().getTransactionValue()) {
              bankSystem.setBalance(this.getNextOnQueueList().getName(), -this.getNextOnQueueList().getTransactionValue());
              bankSystem.setBalance(this.getNextOnQueueList().getTransferDestination(), this.getNextOnQueueList().getTransactionValue());
              console.log(`You successfully transfer ${this.getNextOnQueueList().getTransactionValue()} to ${this.getNextOnQueueList().getTransferDestination()}. Your account's balance is ${bankSystem.getBalance(this.getNextOnQueueList().getName())}.`);
            } else {
              console.log(`Sorry, you dont have sufficient funds in your account.`);
            }
          } else {
            console.log(`Sorry, your transfer destination is not valid.`);
          }
          bankSystem.removeQueueList();
          break;
          }        
      
    } else {
      console.log('No more customer');
    }
  }

  getNextOnQueueList() {
    return bankSystem.getQueueList()[0];
  }
}

let bankSystem = new BankSystem();
let Paijo = new Security('Paijo', 'Ngawi');
let Susi = new Teller('Susi', 'Magetan', 'A1');
let Siti = new Teller('Siti', 'Yogya', 'A2');
let Budi = new Customer('Budi', 'Lampung', 'withdraw', 1000, 2000);
let Ade = new Customer('Ade', 'Purworejo', 'deposit', 2000, 500);
let Joko = new Customer('Joko', 'Pulau Seribu', 'check', 0, 4000);
let Mona = new Customer('Mona', 'Solo', 'transfer', 1000, 2000, 'Joko');


Paijo.introduce();
Susi.introduce();
Budi.introduce();

Paijo.register(Budi);
Paijo.register(Ade);
Paijo.register(Joko);
Paijo.register(Mona);

Susi.serve();
Susi.serve();
Susi.serve();
